<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220915124834 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE responsives_task (responsives_id INT NOT NULL, task_id INT NOT NULL, INDEX IDX_AB9A5367800FF9AA (responsives_id), INDEX IDX_AB9A53678DB60186 (task_id), PRIMARY KEY(responsives_id, task_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE responsives_task ADD CONSTRAINT FK_AB9A5367800FF9AA FOREIGN KEY (responsives_id) REFERENCES responsives (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE responsives_task ADD CONSTRAINT FK_AB9A53678DB60186 FOREIGN KEY (task_id) REFERENCES task (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE responsives_task DROP FOREIGN KEY FK_AB9A5367800FF9AA');
        $this->addSql('ALTER TABLE responsives_task DROP FOREIGN KEY FK_AB9A53678DB60186');
        $this->addSql('DROP TABLE responsives_task');
    }
}
