<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220915095012 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE task_responsives (task_id INT NOT NULL, responsives_id INT NOT NULL, INDEX IDX_EFAA8D6E8DB60186 (task_id), INDEX IDX_EFAA8D6E800FF9AA (responsives_id), PRIMARY KEY(task_id, responsives_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE task_responsives ADD CONSTRAINT FK_EFAA8D6E8DB60186 FOREIGN KEY (task_id) REFERENCES task (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE task_responsives ADD CONSTRAINT FK_EFAA8D6E800FF9AA FOREIGN KEY (responsives_id) REFERENCES responsives (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE task_responsives DROP FOREIGN KEY FK_EFAA8D6E8DB60186');
        $this->addSql('ALTER TABLE task_responsives DROP FOREIGN KEY FK_EFAA8D6E800FF9AA');
        $this->addSql('DROP TABLE task_responsives');
    }
}
