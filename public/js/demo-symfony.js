$(document).ready(function () {
    $('.ajoutertask').on('click', function () {

        new_ligne('tasks', 'projet_task', 'taskn')
    })
    $('.filtrelinkprojet').on('click', function () {

        $('.filtrelinkprojet').parent().parent().parent().parent().find('input').val($(this).html());
        $('.filtrelinkprojet').parent().parent().parent().parent().find('span').val($(this).html());
    })
    $('.filtrelinkstaut').on('click', function () {

        $('.project_status').attr('value', $(this).attr('value'));

    })

    var items = $(".projects__block .projects__item");
    var numItems = items.length;
    var perPage = 8;

    items.slice(perPage).hide();

    $('#pagination-container').pagination({
        items: numItems,
        itemsOnPage: perPage,
        prevText: "&laquo;",
        nextText: "&raquo;",
        onPageClick: function (pageNumber) {
            var showFrom = perPage * (pageNumber - 1);
            var showTo = showFrom + perPage;
            items.hide().slice(showFrom, showTo).show();
        }
    });
});

function new_ligne(source, index, destintation) {
    ind = Number($('#' + index).val()) + 1;
    $ttr = $('#' + source).clone(true);
    $ttr.attr('class', '');
    i = 0;
    tabb = [];
    $ttr.find('input,select,textarea,img').each(function () {
        tab = $(this).attr('table');
        champ = $(this).attr('champ');
        $(this).attr('index', ind);
        $(this).attr('id', champ + ind);
        $(this).attr('name', 'projet[' + tab + '][' + ind + '][' + champ + ']');
        $(this).attr('data-bv-field', 'projet[' + tab + '][' + ind + '][' + champ + ']');
        $(this).val('');
    })
    $('#' + destintation).append($ttr);
    $('#' + destintation).children().last().show();
    $('#' + index).val(ind);

}
