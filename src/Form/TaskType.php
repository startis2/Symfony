<?php

namespace App\Form;

use App\Entity\Status;
use App\Entity\Task;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
           ->add('namet',TextType::class,['attr' =>['class'=>'form-control','champ'=>'name','table'=>'Task','name'=>'','value'=>'a'],'mapped'=>false,'label'=>'Name','required'=>false])
            ->add('imaget', FileType::class,['attr' =>['class'=>'form-control','champ'=>'image','table'=>'Task','name'=>''],'mapped'=>false,'label'=>'Image','required'=>false])
            ->add('Statust' ,EntityType::class,['attr' =>['class'=>'form-control','champ'=>'Status','table'=>'Task','name'=>''],
                'class'=>Status::class,
                'mapped'=>false,
                'label'=>'Status',
                'required'=>false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
        ]);
    }
}
