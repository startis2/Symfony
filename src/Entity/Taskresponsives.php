<?php

namespace App\Entity;

use App\Repository\TaskresponsivesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TaskresponsivesRepository::class)
 */
class Taskresponsives
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=task::class, inversedBy="taskresponsives")
     */
    private $task;

    /**
     * @ORM\ManyToOne(targetEntity=responsives::class, inversedBy="taskresponsives")
     */
    private $responsive;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTask(): ?task
    {
        return $this->task;
    }

    public function setTask(?task $task): self
    {
        $this->task = $task;

        return $this;
    }

//    public function getResponsive(): ?responsives
//    {
//        return $this->responsive;
//    }
//
//    public function setResponsive(?responsives $responsive): self
//    {
//        $this->responsive = $responsive;
//
//        return $this;
//    }
}
