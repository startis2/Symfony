<?php

namespace App\Entity;

use App\Repository\TaskRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TaskRepository::class)
 */
class Task
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="time")
     */
    private $time;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $file;

    /**
     * @ORM\ManyToOne(targetEntity=Status::class, inversedBy="tasks")
     */
    private $statu;

    /**
     * @ORM\ManyToOne(targetEntity=Projet::class, inversedBy="tasks")
     */
    private $projet;

    /**
     * @ORM\ManyToMany(targetEntity=Responsives::class, inversedBy="tasks")
     */
    private $relation;

    /**
     * @ORM\ManyToMany(targetEntity=Responsives::class, mappedBy="task")
     */
    private $responsives;

    public function __construct()
    {
        $this->relation = new ArrayCollection();
        $this->responsives = new ArrayCollection();
    }

//    /**
//     * @ORM\OneToMany(targetEntity=Taskresponsives::class, mappedBy="task")
//     */
//    private $taskresponsives;
//
//    public function __construct()
//    {
//        $this->taskresponsives = new ArrayCollection();
//    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTime(): ?\DateTimeInterface
    {
        return $this->time;
    }

    public function setTime(\DateTimeInterface $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getStatu(): ?status
    {
        return $this->statu;
    }

    public function setStatu(?status $statu): self
    {
        $this->statu = $statu;

        return $this;
    }

    public function getProjet(): ?projet
    {
        return $this->projet;
    }

    public function setProjet(?projet $projet): self
    {
        $this->projet = $projet;

        return $this;
    }

//    /**
//     * @return Collection<int, Taskresponsives>
//     */
//    public function getTaskresponsives(): Collection
//    {
//        return $this->taskresponsives;
//    }

//    public function addTaskresponsife(Taskresponsives $taskresponsife): self
//    {
//        if (!$this->taskresponsives->contains($taskresponsife)) {
//            $this->taskresponsives[] = $taskresponsife;
//            $taskresponsife->setTask($this);
//        }
//
//        return $this;
//    }

//    public function removeTaskresponsife(Taskresponsives $taskresponsife): self
//    {
//        if ($this->taskresponsives->removeElement($taskresponsife)) {
//            // set the owning side to null (unless already changed)
//            if ($taskresponsife->getTask() === $this) {
//                $taskresponsife->setTask(null);
//            }
//        }
//
//        return $this;
//    }

    /**
     * @return Collection<int, Responsives>
     */
    public function getRelation(): Collection
    {
        return $this->relation;
    }



    public function addRelation(Responsives $relation): self
    {
        if (!$this->relation->contains($relation)) {
            $this->relation[] = $relation;
        }

        return $this;
    }

    public function removeRelation(Responsives $relation): self
    {
        $this->relation->removeElement($relation);

        return $this;
    }

    /**
     * @return Collection<int, Responsives>
     */
    public function getResponsives(): Collection
    {
        return $this->responsives;
    }

    public function addResponsife(Responsives $responsife): self
    {
        if (!$this->responsives->contains($responsife)) {
            $this->responsives[] = $responsife;
            $responsife->addTask($this);
        }

        return $this;
    }

    public function removeResponsife(Responsives $responsife): self
    {
        if ($this->responsives->removeElement($responsife)) {
            $responsife->removeTask($this);
        }

        return $this;
    }

}

