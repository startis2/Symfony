<?php

namespace App\Entity;

use App\Repository\UsersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UsersRepository::class)
 */
class Users
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $iamge;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @ORM\ManyToMany(targetEntity=Usergroups::class, inversedBy="users")
     */
    private $usergroup;

    public function __construct()
    {
        $this->usergroup = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIamge(): ?string
    {
        return $this->iamge;
    }

    public function setIamge(?string $iamge): self
    {
        $this->iamge = $iamge;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection<int, Usergroups>
     */
    public function getUsergroup(): Collection
    {
        return $this->usergroup;
    }

    public function addUsergroup(Usergroups $usergroup): self
    {
        if (!$this->usergroup->contains($usergroup)) {
            $this->usergroup[] = $usergroup;
        }

        return $this;
    }

    public function removeUsergroup(Usergroups $usergroup): self
    {
        $this->usergroup->removeElement($usergroup);

        return $this;
    }
}
