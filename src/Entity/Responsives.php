<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ResponsivesRepository::class)
 */
class Responsives
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Taskresponsives::class, mappedBy="responsive")
     */
    private $taskresponsives;

    /**
     * @ORM\ManyToMany(targetEntity=Task::class, mappedBy="relation")
     */
    private $tasks;

    /**
     * @ORM\ManyToMany(targetEntity=Task::class, inversedBy="responsives")
     */
    private $task;

    public function __construct()
    {
        $this->taskresponsives = new ArrayCollection();
        $this->tasks = new ArrayCollection();
        $this->task = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

//    /**
//     * @return Collection<int, Taskresponsives>
//     */
//    public function getTaskresponsives(): Collection
//    {
//        return $this->taskresponsives;
//    }
//
//    public function addTaskresponsife(Taskresponsives $taskresponsife): self
//    {
//        if (!$this->taskresponsives->contains($taskresponsife)) {
//            $this->taskresponsives[] = $taskresponsife;
//            $taskresponsife->setResponsive($this);
//        }
//
//        return $this;
//    }
//
//    public function removeTaskresponsife(Taskresponsives $taskresponsife): self
//    {
//        if ($this->taskresponsives->removeElement($taskresponsife)) {
//            // set the owning side to null (unless already changed)
//            if ($taskresponsife->getResponsive() === $this) {
//                $taskresponsife->setResponsive(null);
//            }
//        }
//
//        return $this;
//    }

/**
 * @return Collection<int, Task>
 */
public function getTasks(): Collection
{
    return $this->tasks;
}

public function addTask(Task $task): self
{
    if (!$this->tasks->contains($task)) {
        $this->tasks[] = $task;
        $task->addRelation($this);
    }

    return $this;
}

public function removeTask(Task $task): self
{
    if ($this->tasks->removeElement($task)) {
        $task->removeRelation($this);
    }

    return $this;
}

/**
 * @return Collection<int, Task>
 */
public function getTask(): Collection
{
    return $this->task;
}
}
