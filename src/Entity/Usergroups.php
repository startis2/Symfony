<?php

namespace App\Entity;

use App\Repository\UsergroupsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UsergroupsRepository::class)
 */
class Usergroups
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $isclient;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $isadmin;

    /**
     * @ORM\ManyToMany(targetEntity=Users::class, mappedBy="usergroup")
     */
    private $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsclient(): ?string
    {
        return $this->isclient;
    }

    public function setIsclient(?string $isclient): self
    {
        $this->isclient = $isclient;

        return $this;
    }

    public function getIsadmin(): ?string
    {
        return $this->isadmin;
    }

    public function setIsadmin(?string $isadmin): self
    {
        $this->isadmin = $isadmin;

        return $this;
    }

    /**
     * @return Collection<int, Users>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(Users $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addUsergroup($this);
        }

        return $this;
    }

    public function removeUser(Users $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeUsergroup($this);
        }

        return $this;
    }
}
