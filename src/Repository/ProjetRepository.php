<?php

namespace App\Repository;

use App\Entity\Projet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Projet>
 *
 * @method Projet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Projet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Projet[]    findAll()
 * @method Projet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Projet::class);
    }

    public function add(Projet $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Projet $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByFilter($keyword3, $status, $value): array
    {
        $projet = $this->createQueryBuilder('p');
        if ($status != '') {
            $projet->Where('p.status = :val')
                ->setParameter('val', $status);
        }
        if ($value != '') {
            $projet->andWhere('p.name LIKE :value')
                ->setParameter('value', $value);
        }
        if ($keyword3 != '') {
            $projet->andWhere('p.name LIKE :url')
                ->setParameter('url', '%' . $keyword3 . '%');
        }
        return $projet
            ->getQuery()
            ->getResult();
    }

}
