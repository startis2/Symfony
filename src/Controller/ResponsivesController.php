<?php

namespace App\Controller;

use App\Entity\Responsives;
use App\Form\ResponsivesType;
use App\Repository\ResponsivesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/responsives")
 */
class ResponsivesController extends AbstractController
{
    /**
     * @Route("/", name="app_responsives_index", methods={"GET"})
     */
    public function index(ResponsivesRepository $responsivesRepository): Response
    {
        return $this->render('responsives/index.html.twig', [
            'responsives' => $responsivesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_responsives_new", methods={"GET", "POST"})
     */
    public function new(Request $request, ResponsivesRepository $responsivesRepository): Response
    {
        $responsife = new Responsives();
        $form = $this->createForm(ResponsivesType::class, $responsife);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $responsivesRepository->add($responsife, true);

            return $this->redirectToRoute('app_responsives_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('responsives/new.html.twig', [
            'responsife' => $responsife,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_responsives_show", methods={"GET"})
     */
    public function show(Responsives $responsife): Response
    {
        return $this->render('responsives/show.html.twig', [
            'responsife' => $responsife,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_responsives_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Responsives $responsife, ResponsivesRepository $responsivesRepository): Response
    {
        $form = $this->createForm(ResponsivesType::class, $responsife);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $responsivesRepository->add($responsife, true);

            return $this->redirectToRoute('app_responsives_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('responsives/edit.html.twig', [
            'responsife' => $responsife,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_responsives_delete", methods={"POST"})
     */
    public function delete(Request $request, Responsives $responsife, ResponsivesRepository $responsivesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$responsife->getId(), $request->request->get('_token'))) {
            $responsivesRepository->remove($responsife, true);
        }

        return $this->redirectToRoute('app_responsives_index', [], Response::HTTP_SEE_OTHER);
    }
}
