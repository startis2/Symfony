<?php

namespace App\Controller;

use App\Entity\Projet;
use App\Entity\Task;
use App\Form\ProjetType;
use App\Form\TaskType;
use App\Repository\ProjetRepository;
use App\Repository\StatusRepository;
use App\Repository\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/projet")
 */
class ProjetController extends AbstractController
{
    /**
     * @Route("/", name="app_projet_index", methods={"GET"})
     */
    public function index(ProjetRepository $projetRepository, StatusRepository $statusRepository, Request $request, string $save = 'i'): Response
    {
        $projet = $projetRepository->findAll();
        $projetall = $projetRepository->findAll();
        $mode = 'grid';
        $keyword3 = '';
        $value = '';
        $status = '';
        $save = 'i';

        if ($request->isMethod('POST')) {
            $mode = $_GET['mode'];
        }
        if (isset($_GET['porject_name'])) {
            $value = $_GET['porject_name'];
        }
        if (isset($_GET['keyword3'])) {
            $keyword3 = $_GET['keyword3'];
        }
        if (isset($_GET['stauts_name'])) {
            $status = $_GET['stauts_name'];
        }
        if (isset($_GET['save'])) {
            $save = $_GET['save'];
        }
        $projet = $projetRepository->findByFilter($keyword3, $status, $value);
        foreach ($projet as $key => $entity) {
            $entity->img = base64_encode(stream_get_contents($entity->getImage()));
        }

        return $this->render('projet/index.html.twig', [
            'projets' => $projet,
            'projetall' => $projetall,
            'mode' => $mode,
            'keyword3' => $keyword3,
            'status' => $statusRepository->findAll(),
            'save' => $save,
        ]);
    }

    /**
     * @Route("/new", name="app_projet_new", methods={"GET", "POST"})
     */
    public function new(Request $request, ProjetRepository $projetRepository, TaskRepository $taskRepository, StatusRepository $statusRepository): Response
    {
        $projet = new Projet();
        $form = $this->createForm(ProjetType::class, $projet);
        $task = new Task();
        $formt = $this->createForm(TaskType::class, $task);
        $projet->addTask($task);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            try {
                $file = $projet->getImage();
                $uploads_directory = $this->getParameter('uploads_directory');
                $filename = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                    $uploads_directory,
                    $filename
                );
                $path = $uploads_directory . '/' . $filename;
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);
                $projet->setImage($data);
                $projets = new Projet();
                $projets->setImage($projet->getImage());
                $projets->setDate($projet->getDate());
                $projets->setName($projet->getName());
                $projets->setTask($projet->getTask());
                $projets->setStatus($projet->getStatus());
                $projetRepository->add($projets, true);
                $pro = $projets->getId();
                $tasks = $request->request->get('projet')['Task'];
                $tasksImage = $request->files->get('projet')['Task'];
                foreach ($tasks as $v => $t) {
                    $taskImage = $tasksImage[$v]['image'];
                    $uploads_directory = $this->getParameter('uploads_directory');
                    $imageName = md5(uniqid()) . '.' . $taskImage->guessExtension();
                    $taskImage->move(
                        $uploads_directory,
                        $imageName
                    );

                    $statusObject = $statusRepository->find($t['Status']);
                    $taskObject = new task();
                    $taskObject->setName($t['name']);
                    $taskObject->setStatu($statusObject);
                    $taskObject->setFile($imageName);
                    $taskObject->setProjet($projets);
                    $taskObject->setDate(\DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
                    $taskObject->setTime(\DateTime::createFromFormat('H:i:s', date('H:i:s')));
                    $taskRepository->add($taskObject, true);
                }
                $save = 's';

            } catch (Exception $e) {
                $save = 'e';
            }

            return $this->redirectToRoute('app_projet_index', array('save' => $save), Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('projet/new.html.twig', [
            'projet' => $projet,
            'form' => $form,
            'formt' => $formt,
        ]);
    }

    /**
     * @Route("/{id}", name="app_projet_show", methods={"GET"})
     */
    public function show(Projet $projet): Response
    {
        $id = $projet->getId();
        $task = $this->getDoctrine()->getManager()
            ->getRepository(Task::class)
            ->findBy(['projet' => $id]);
        foreach ($task as $key => $t) {
            $t->statu_name = $t->getStatu()->getName();
            $task_id = $t->getID();
        }
        $projet->img = base64_encode(stream_get_contents($projet->getImage()));

        return $this->render('projet/show.html.twig', [
            'projet' => $projet,
            'tasks' => $task
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_projet_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Projet $projet, ProjetRepository $projetRepository): Response
    {
        $form = $this->createForm(ProjetType::class, $projet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $projetRepository->add($projet, true);

            return $this->redirectToRoute('app_projet_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('projet/edit.html.twig', [
            'projet' => $projet,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_projet_delete", methods={"POST"})
     */
    public function delete(Request $request, Projet $projet, ProjetRepository $projetRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $projet->getId(), $request->request->get('_token'))) {
            $projetRepository->remove($projet, true);
        }

        return $this->redirectToRoute('app_projet_index', [], Response::HTTP_SEE_OTHER);
    }
}
