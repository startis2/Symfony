<?php

namespace App\Controller;

use App\Entity\Usergroups;
use App\Form\UsergroupsType;
use App\Repository\UsergroupsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/usergroups")
 */
class UsergroupsController extends AbstractController
{
    /**
     * @Route("/", name="app_usergroups_index", methods={"GET"})
     */
    public function index(UsergroupsRepository $usergroupsRepository): Response
    {
        return $this->render('usergroups/index.html.twig', [
            'usergroups' => $usergroupsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_usergroups_new", methods={"GET", "POST"})
     */
    public function new(Request $request, UsergroupsRepository $usergroupsRepository): Response
    {
        $usergroup = new Usergroups();
        $form = $this->createForm(UsergroupsType::class, $usergroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usergroupsRepository->add($usergroup, true);

            return $this->redirectToRoute('app_usergroups_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('usergroups/new.html.twig', [
            'usergroup' => $usergroup,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_usergroups_show", methods={"GET"})
     */
    public function show(Usergroups $usergroup): Response
    {
        return $this->render('usergroups/show.html.twig', [
            'usergroup' => $usergroup,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_usergroups_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Usergroups $usergroup, UsergroupsRepository $usergroupsRepository): Response
    {
        $form = $this->createForm(UsergroupsType::class, $usergroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usergroupsRepository->add($usergroup, true);

            return $this->redirectToRoute('app_usergroups_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('usergroups/edit.html.twig', [
            'usergroup' => $usergroup,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_usergroups_delete", methods={"POST"})
     */
    public function delete(Request $request, Usergroups $usergroup, UsergroupsRepository $usergroupsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$usergroup->getId(), $request->request->get('_token'))) {
            $usergroupsRepository->remove($usergroup, true);
        }

        return $this->redirectToRoute('app_usergroups_index', [], Response::HTTP_SEE_OTHER);
    }
}
