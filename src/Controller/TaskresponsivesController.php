<?php

namespace App\Controller;

use App\Entity\Taskresponsives;
use App\Form\TaskresponsivesType;
use App\Repository\TaskresponsivesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/taskresponsives")
 */
class TaskresponsivesController extends AbstractController
{
    /**
     * @Route("/", name="app_taskresponsives_index", methods={"GET"})
     */
    public function index(TaskresponsivesRepository $taskresponsivesRepository): Response
    {
        return $this->render('taskresponsives/index.html.twig', [
            'taskresponsives' => $taskresponsivesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_taskresponsives_new", methods={"GET", "POST"})
     */
    public function new(Request $request, TaskresponsivesRepository $taskresponsivesRepository): Response
    {
        $taskresponsife = new Taskresponsives();
        $form = $this->createForm(TaskresponsivesType::class, $taskresponsife);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $taskresponsivesRepository->add($taskresponsife, true);

            return $this->redirectToRoute('app_taskresponsives_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('taskresponsives/new.html.twig', [
            'taskresponsife' => $taskresponsife,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_taskresponsives_show", methods={"GET"})
     */
    public function show(Taskresponsives $taskresponsife): Response
    {
        return $this->render('taskresponsives/show.html.twig', [
            'taskresponsife' => $taskresponsife,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_taskresponsives_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Taskresponsives $taskresponsife, TaskresponsivesRepository $taskresponsivesRepository): Response
    {
        $form = $this->createForm(TaskresponsivesType::class, $taskresponsife);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $taskresponsivesRepository->add($taskresponsife, true);

            return $this->redirectToRoute('app_taskresponsives_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('taskresponsives/edit.html.twig', [
            'taskresponsife' => $taskresponsife,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_taskresponsives_delete", methods={"POST"})
     */
    public function delete(Request $request, Taskresponsives $taskresponsife, TaskresponsivesRepository $taskresponsivesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$taskresponsife->getId(), $request->request->get('_token'))) {
            $taskresponsivesRepository->remove($taskresponsife, true);
        }

        return $this->redirectToRoute('app_taskresponsives_index', [], Response::HTTP_SEE_OTHER);
    }
}
