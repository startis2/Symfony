<?php

namespace App\Controller;

use App\Entity\Visibles;
use App\Form\VisiblesType;
use App\Repository\VisiblesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/visibles")
 */
class VisiblesController extends AbstractController
{
    /**
     * @Route("/", name="app_visibles_index", methods={"GET"})
     */
    public function index(VisiblesRepository $visiblesRepository): Response
    {
        return $this->render('visibles/index.html.twig', [
            'visibles' => $visiblesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_visibles_new", methods={"GET", "POST"})
     */
    public function new(Request $request, VisiblesRepository $visiblesRepository): Response
    {
        $visible = new Visibles();
        $form = $this->createForm(VisiblesType::class, $visible);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $visiblesRepository->add($visible, true);

            return $this->redirectToRoute('app_visibles_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('visibles/new.html.twig', [
            'visible' => $visible,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_visibles_show", methods={"GET"})
     */
    public function show(Visibles $visible): Response
    {
        return $this->render('visibles/show.html.twig', [
            'visible' => $visible,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_visibles_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Visibles $visible, VisiblesRepository $visiblesRepository): Response
    {
        $form = $this->createForm(VisiblesType::class, $visible);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $visiblesRepository->add($visible, true);

            return $this->redirectToRoute('app_visibles_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('visibles/edit.html.twig', [
            'visible' => $visible,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_visibles_delete", methods={"POST"})
     */
    public function delete(Request $request, Visibles $visible, VisiblesRepository $visiblesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$visible->getId(), $request->request->get('_token'))) {
            $visiblesRepository->remove($visible, true);
        }

        return $this->redirectToRoute('app_visibles_index', [], Response::HTTP_SEE_OTHER);
    }
}
